(function () {
    "use strict";

    if (!_) {
        throw new Error("lodash library is required");
    }

// Const --------------------------------------------------------------------------------------------------------- start

    const CARD_REGEX = /^[2-9|T|J|Q|K|A]{1}[S|H|D|C]{1}$/;

// Const --------------------------------------------------------------------------------------------------------- end


// Enums --------------------------------------------------------------------------------------------------------- start

    const CardValue = Object.freeze({
        "2":0,
        "3":1,
        "4":2,
        "5":3,
        "6":4,
        "7":5,
        "8":6,
        "9":7,
        "T":8,
        "J":9,
        "Q":10,
        "K":11,
        "A":12
    });
    const CardSuit = Object.freeze({
        "S":0,
        "H":1,
        "D":2,
        "C":3
    });
    const CompareResult = Object.freeze({
        "WIN":1,
        "LOSS":2,
        "Tie":3
    });
    const HandRank = Object.freeze({
        "HIGH_CARD":1,
        "PAIR":2,
        "TWO_PAIRS":3,
        "THREE_OF_KIND":4,
        "STRAIGHT":5,
        "FLUSH":6,
        "FULL_HOUSE":7,
        "FOUR_OF_A_KIND":8,
        "STRAIGHT_FLUSH":9,
        "ROYAL_FLUSH":10
    });

// Enums --------------------------------------------------------------------------------------------------------- end


// Models -------------------------------------------------------------------------------------------------------- start

    class Card {
        constructor(cardString) {
            if (!CARD_REGEX.test(cardString)) {
                throw new Error("Card '" + cardString + "' is invalid.");
            }
            this.value = CardValue[cardString[0]];
            this.suit = CardSuit[cardString[1]];
        }
    }

    class PokerHand {
        constructor(handString) {
            this.cards = this.parseHandString(handString);
            let { rank, weight } = this._resolveRankAndWeight(this.cards);
            this.rank = rank;
            this.weight = weight;
        }

        parseHandString(handString) {
            let stringCards = handString.split(" ");

            if (stringCards.length !== 5) {
                throw new Error("Hand '"+handString+"' is not correct.");
            }

            // filter invalid cards
            let invalidCards = stringCards.filter(card => !CARD_REGEX.test(card));
            if (invalidCards.length !== 0) {
                throw new Error(
                    "Cards [" + invalidCards.join(", ") + "] are invalid. Card template is " + CARD_REGEX.toString()
                );
            }

            // cards shouldn't be repeatable
            if (_.uniq(stringCards).length !== 5) {
                throw new Error("Cards can not be repeated.");
            }

            //create list of cards
            return stringCards.map(cardString => new Card(cardString));
        };

        compareWith(pokerHand) {
            let weightDifference = this.weight - pokerHand.weight;

            if(weightDifference > 0) {
                return CompareResult.WIN;
            } else if (weightDifference < 0) {
                return CompareResult.LOSS;
            } else {
                return CompareResult.Tie;
            }
        }

        _resolveRankAndWeight(cards) {
            this._sortedCards = _.clone(cards);
            this._sortedCards.sort((card1, card2) => card1.value - card2.value);
            this._groupCardsBySuit = _.groupBy(this._sortedCards, card => card.suit);
            this._groupCardsByValue = _.groupBy(this._sortedCards, card => card.value);

            let rank = this._resolveRank();
            let weight = this._calculateWeight(rank);

            return { rank, weight };
        }

        _resolveRank() {
            let resolvers = new Map([
                [HandRank.ROYAL_FLUSH, this._isRoyalFlush],
                [HandRank.STRAIGHT_FLUSH, this._isStraightFlush],
                [HandRank.FOUR_OF_A_KIND, this._isFourOfKind],
                [HandRank.FULL_HOUSE, this._isFullHouse],
                [HandRank.FLUSH, this._isFlush],
                [HandRank.STRAIGHT, this._isStraight],
                [HandRank.THREE_OF_KIND, this._isThreeOfKind],
                [HandRank.TWO_PAIRS, this._isTwoPairs],
                [HandRank.PAIR, this._isPair],
                [HandRank.HIGH_CARD, this._isHighCard],
            ]);

            let iterator = resolvers.entries();
            let handRank = null;
            do {
                let [rank, checker] = iterator.next().value;
                if (checker.call(this)) {
                    handRank = rank;
                }
            } while (handRank == null);

            return handRank;
        }

        _isRoyalFlush() {
            let isAllInOneSuit = _(this._groupCardsBySuit).values().some({ length: 5 });
            if (!isAllInOneSuit) {
                return false;
            }
            return this._sortedCards[0].suit === CardValue.T;
        }

        _isStraightFlush() {
            let isAllInOneSuit = _(this._groupCardsBySuit).values().some({ length: 5 });
            if (!isAllInOneSuit) {
                return false;
            }
            return this._isSequence(this._sortedCards);
        }

        _isFourOfKind() {
            return _(this._groupCardsByValue).values().some({ length: 4 });
        }

        _isFullHouse() {
            let isThreeSameValue = _(this._groupCardsByValue).values().some({ length: 3 });
            let isTwoSameValue = _(this._groupCardsByValue).values().some({ length: 2 });
            return isThreeSameValue && isTwoSameValue;
        }

        _isFlush() {
            return _(this._groupCardsBySuit).values().some({ length: 5 });
        }

        _isStraight() {
            return this._isSequence(this._sortedCards);
        }

        _isThreeOfKind() {
            return _(this._groupCardsByValue).values().some({ length: 3 });
        }

        _isTwoPairs() {
            return _(this._groupCardsByValue).values().filter({ length: 2 }).size() === 2;
        }

        _isPair() {
            return _(this._groupCardsByValue).values().some({ length: 2 })
        }

        _isHighCard() {
            return true;
        }

        _isSequence(cards) {
            for (let i = 0; i < cards.length - 1; i++) {
                if (cards[i].value + 1 !== cards[i + 1].value) {
                    return false;
                }
            }

            return true;
        }

        _calculateWeight(rank) {
            const maxValue = _(CardValue).values().max();
            let weight = rank * Math.pow(maxValue, this._sortedCards.length + 10); // rank is more important then position and value
            switch(rank) {
                case HandRank.FOUR_OF_A_KIND: {
                    let duplicatedValue = this._getDuplicatedValue(4);
                    weight += duplicatedValue * Math.pow(maxValue, this._sortedCards.length + 1);
                    let otherCards = _.filter(this._sortedCards, card => card.value !== duplicatedValue);
                    return weight + this._calculateWeightByIndex(otherCards);
                }
                case HandRank.FULL_HOUSE: {
                    let valueThreeCards = this._getDuplicatedValue(3);
                    let valueTwoCards = this._getDuplicatedValue(2);
                    weight += valueThreeCards * Math.pow(maxValue, this._sortedCards.length + 1);
                    weight += valueTwoCards * Math.pow(maxValue, this._sortedCards.length);
                    let otherCards = _.filter(this._sortedCards, c => {
                        return c.value !== valueThreeCards && c.value !== valueThreeCards
                    });
                    return weight + this._calculateWeightByIndex(otherCards);
                }
                case HandRank.THREE_OF_KIND: {
                    let duplicatedValue = this._getDuplicatedValue(3);
                    weight += duplicatedValue * Math.pow(maxValue, this._sortedCards.length + 1);
                    let otherCards = _.filter(this._sortedCards, c => c.value !== duplicatedValue);
                    return weight + this._calculateWeightByIndex(otherCards);
                }
                case HandRank.TWO_PAIRS: {
                    let firstDuplicatedValue = this._getDuplicatedValue(2);
                    delete this._groupCardsByValue[firstDuplicatedValue];
                    let secondDuplicatedValue = this._getDuplicatedValue(2);
                    weight += firstDuplicatedValue * Math.pow(maxValue, this._sortedCards.length + 1);
                    weight += secondDuplicatedValue * Math.pow(maxValue, this._sortedCards.length);
                    let otherCards = _.filter(this._sortedCards, c => {
                        return c.value !== firstDuplicatedValue && c.value !== secondDuplicatedValue
                    });
                    return weight + this._calculateWeightByIndex(otherCards);
                }
                case HandRank.PAIR: {
                    let duplicatedValue = this._getDuplicatedValue(2);
                    weight += duplicatedValue * Math.pow(maxValue, this._sortedCards.length + 1);
                    let otherCards = _.filter(this._sortedCards, c => c.value !== duplicatedValue);
                    return weight + this._calculateWeightByIndex(otherCards);
                }
                default:
                    return weight + this._calculateWeightByIndex(this._sortedCards);
            }
        }

        _getDuplicatedValue(numberOfRepetitions) {
            return _(this._groupCardsByValue)
                .values()
                .filter({ length: numberOfRepetitions })
                .flatten()
                .map(i => i.value)
                .max();
        }

        _calculateWeightByIndex(cards) {
            const maxValue = _(CardValue).values().max();
            return _.reduce(cards, (weight, card, index) => weight + card.value * Math.pow(maxValue, index + 1), 0);
        }
    }

// Models -------------------------------------------------------------------------------------------------------- end

    window.PokerHand = PokerHand;
    window.Card = Card;
})();
