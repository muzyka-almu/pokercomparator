const CardValue = Object.freeze({
    "2":0,
    "3":1,
    "4":2,
    "5":3,
    "6":4,
    "7":5,
    "8":6,
    "9":7,
    "T":8,
    "J":9,
    "Q":10,
    "K":11,
    "A":12
});
const CardSuit = Object.freeze({
    "S":0,
    "H":1,
    "D":2,
    "C":3
});
const CompareResult = Object.freeze({
    "WIN":1,
    "LOSS":2,
    "Tie":3
});
const HandRank = Object.freeze({
    "HIGH_CARD":1,
    "PAIR":2,
    "TWO_PAIRS":3,
    "THREE_OF_KIND":4,
    "STRAIGHT":5,
    "FLUSH":6,
    "FULL_HOUSE":7,
    "FOUR_OF_A_KIND":8,
    "STRAIGHT_FLUSH":9,
    "ROYAL_FLUSH":10
});

describe("[Poker]", function() {
    it("should be defined PokerHand and Card", function () {
        expect(PokerHand).toBeDefined();
        expect(Card).toBeDefined();
    });

    describe("[Card]", function() {
        it("should throw error on create if string is invalid", function () {
            try {
                new Card("invalid");
                fail("Method should fail");
            } catch(err) {
                expect(err.message).toEqual("Card 'invalid' is invalid.");
            }
        });

        it("should set correct suit and value", function () {
            let cardString = "7H";
            let card = new Card(cardString);

            expect(card.value).toBe(CardValue[cardString[0]]);
            expect(card.suit).toBe(CardSuit[cardString[1]]);
        });
    });

    describe("[PokerHand]", function() {
        it("should throw error on create if string is invalid", function () {
            try {
                new PokerHand("invalid");
                fail("Method should fail");
            } catch(err) {
                expect(err.message).toEqual("Hand 'invalid' is not correct.");
            }
        });

        it("should throw error on create if some cards are invalid", function () {
            try {
                new PokerHand("4S TC LL KD KC");
                fail("Method should fail");
            } catch(err) {
                expect(err.message).toEqual("Cards [LL] are invalid. Card template is /^[2-9|T|J|Q|K|A]{1}[S|H|D|C]{1}$/");
            }
        });

        it("should set cards and calculate weight on create event", function () {
            let expectCards = [
                { value: CardValue.T, suit: CardSuit.C },
                { value: CardValue.J, suit: CardSuit.D },
                { value: CardValue.K, suit: CardSuit.S },
                { value: CardValue.Q, suit: CardSuit.C },
                { value: CardValue.T, suit: CardSuit.C },
            ];
            let expectRank = HandRank.PAIR;
            let expectWeight = 30814043173077732;
            spyOn(PokerHand.prototype, "_resolveRankAndWeight").and.returnValue({ rank: expectRank, weight: expectWeight });
            spyOn(PokerHand.prototype, "parseHandString").and.returnValue(expectCards);
            let hand = new PokerHand("TC JD KS QC TC");

            expect(hand.cards).toEqual(expectCards);
            expect(hand.weight).toBe(expectWeight);
            expect(hand.rank).toBe(expectRank);
        });

        it("should correct return result of comparisons", function () {
            let compareWith = PokerHand.prototype.compareWith;

            expect(compareWith.call({ weight: 20 }, { weight: 10 })).toBe(CompareResult.WIN);
            expect(compareWith.call({ weight: 10 }, { weight: 20 })).toBe(CompareResult.LOSS);
            expect(compareWith.call({ weight: 10 }, { weight: 10 })).toBe(CompareResult.Tie);
        });
    });

    describe("[Integration]", function() {
        let handTestCases = [
            { hand1: "TS JS QS KS AS", hand2: "9S JS QS KS AS", result: 1 },
            { hand1: "2S JS QS KS AS", hand2: "TC JC QC KC AC", result: 2 },
            { hand1: "TS JS QS KS AS", hand2: "TC JC QC KC AC", result: 3 },

            { hand1: "2S 3S 4S 5S 6S", hand2: "8S 2S 3S 4S 5S", result: 1 },
            { hand1: "2S 3S 4S 5S 6S", hand2: "3S 4S 5S 6S 7S", result: 2 },
            { hand1: "3S 4S 5S 6S 7S", hand2: "3C 4C 5C 6C 7C", result: 3 },

            { hand1: "TS TC TD TH 6S", hand2: "TS TC 3S 4S 5S", result: 1 },
            { hand1: "TS TC TD TH 6S", hand2: "TS TC TD TH 7D", result: 2 },
            { hand1: "TS TC TD TH 6S", hand2: "TS TC TD TH 6C", result: 3 },

            { hand1: "2S 2C QD QH QS", hand2: "2D 2C JC JD JH", result: 1 },
            { hand1: "2D 2C 3C 3D 3H", hand2: "4S 4C 3H 3S 3D", result: 2 },
            { hand1: "2S 2D 3C 3S 3H", hand2: "2H 2C 3D 3C 3H", result: 3 },

            { hand1: "KS 3S 4S 2S 5S", hand2: "2S 9S TS 7S 8S", result: 1 },
            { hand1: "KS QS TS JS 8S", hand2: "2C 3C AC 4C 5C", result: 2 },
            { hand1: "2S 3S 4S 5S 6S", hand2: "2C 3C 4C 5C 6C", result: 3 },

            { hand1: "6S 3C 4D 7H 5S", hand2: "2S 5H 6H 4H 3C", result: 1 },
            { hand1: "2H 4C 3S 5S 6S", hand2: "JH TC AC QS KC", result: 2 },
            { hand1: "2H 4C 3S 5S 6S", hand2: "4S 2C 6H 5C 3S", result: 3 },

            { hand1: "9S 9C 9D 7H 5S", hand2: "7S 7H 7D 8H 4C", result: 1},
            { hand1: "9S 9C 9D 8C 3S", hand2: "9S 9C 9D 8H 4S", result: 2},
            { hand1: "9S 9C 9D 8D 7C", hand2: "9S 9H 9C 7C 8D", result: 3},

            { hand1: "9S 9C 8D 8H 2S", hand2: "9S 9H 7D 7H 3C", result: 1 },
            { hand1: "9S 9C 8D 8H 3S", hand2: "9S 9C 8D 8H 2S", result: 1 },
            { hand1: "7S 7C 5D 5H 2S", hand2: "8S 8C 5D 5H 2S", result: 2 },
            { hand1: "8S 8C 5D 5H 2S", hand2: "8C 8S 5C 5H 2S", result: 3 },

            { hand1: "9S 9C 6D 8H 2S", hand2: "9S 9H 3D 7H 4C", result: 1 },
            { hand1: "5S 5C 4D 3H 2S", hand2: "7S 7C 5D 4H 2S", result: 2 },
            { hand1: "5S 5C 4D 3H 2S", hand2: "5S 5C 4D 3H 2S", result: 3 },

            { hand1: "AS 2C 3D 6H 2S", hand2: "9S 8H 3D 7H 4C", result: 1 },
            { hand1: "AS 2C 3D 6H 2S", hand2: "AS 2C 4D 6H 2S", result: 2 },
            { hand1: "AS 2C 3D 6H 2S", hand2: "AD 2S 3C 6H 2D", result: 3 },

            { hand1: "TS JS QS KS AS", hand2: "2S 3S 4S 5S 6S", result: 1 },
            { hand1: "2S 3S 4S 5S 6S", hand2: "TS TC TD TH 6S", result: 1 },
            { hand1: "TS TC TD TH 6S", hand2: "2S 2C QD QH QS", result: 1 },
            { hand1: "2S 2C QD QH QS", hand2: "KS 3S 4S 2S 5S", result: 1 },
            { hand1: "KS 3S 4S 2S 5S", hand2: "6S 3C 4D 7H 5S", result: 1 },
            { hand1: "6S 3C 4D 7H 5S", hand2: "9S 9C 9D 7H 5S", result: 1 },
            { hand1: "9S 9C 9D 7H 5S", hand2: "9S 9C 8D 8H 2S", result: 1 },
            { hand1: "9S 9C 8D 8H 2S", hand2: "9S 9C 6D 8H 2S", result: 1 },
            { hand1: "9S 9C 6D 8H 2S", hand2: "AS 2C 3D 6H 2S", result: 1 }
        ];

        it("should correctly compare two hands", function () {
            _.each(handTestCases, testCase => {
                let hand1 = new PokerHand(testCase.hand1);
                let hand2 = new PokerHand(testCase.hand2);
                expect(hand1.compareWith(hand2)).toBe(testCase.result);
            });
        });
    });
});
