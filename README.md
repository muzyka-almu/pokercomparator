# Poker hands comparator

A simple program that allows you to compare poker hands and determines a winner.

## Initial setup

Just download the source code to your directory `git clone https://muzyka-almu@bitbucket.org/muzyka-almu/pokercomparator.git`
And open `./pokercomparator/poker.html` file in your browser.

## Using the application

When the page is loaded enter two hands in inputs and push `Compare` button.
The result will be displayed above

## Testing

Tests located in file `./pokercomparator/src/main.spec.js`.
To run them, just open file `./pokercomparator/test.html` in your browser.
